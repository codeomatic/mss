from game import Game
from threading import Event
import keyboard

stopFlag = Event()
game = Game(stopFlag)
game.start()

VALID_INPUTS = ['w', 'a', 's', 'd']

while True:
    text = keyboard.read_key()

    if text == 'n':
        stopFlag.set()
        stopFlag = Event()
        game = Game(stopFlag)
        game.start()
    elif text in VALID_INPUTS:
        game.set_user_action(text)