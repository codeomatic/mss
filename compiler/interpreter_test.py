import unittest
from nodes import *
from interpreter import Interpreter
from values import Number, Bool

class TestInterpreter(unittest.TestCase):

    def test_empty(self):
        with self.assertRaises(Exception):
            value = Interpreter().visit(None)
        
    def test_number(self):
        value = Interpreter().visit(NumberNode(21.2))

        self.assertEqual(value,Number(21.2))

    def test_expression(self):
        # 5 - 3/3 * 10.0
        value = Interpreter().visit(
            SubtractNode(
                NumberNode(5),
                MultiplyNode(
                    DivideNode(
                        NumberNode(3),
                        NumberNode(3)
                    ),
                    NumberNode(10)
                )
            )
        )

        self.assertEqual(value, Number(-5))

    def test_bool_expression(self):
        # 5 - 3/3 * 10.0 < 0
        value = Interpreter().visit(
            BoolNodeLT(
                SubtractNode(
                    NumberNode(5),
                    MultiplyNode(
                        DivideNode(
                            NumberNode(3),
                            NumberNode(3)
                        ),
                        NumberNode(10)
                    )
                ),
                NumberNode(0)
            )
        )

        self.assertEqual(value, Bool(True))