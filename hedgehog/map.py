import math
from random import randint

class Point:
    def __init__(self,x_init,y_init):
        self.x = x_init
        self.y = y_init

    def __eq__(self, other): 
        if not isinstance(other, Point):
            return False
        else:
            return self.x == other.x and self.y == other.y

    def dist_to(self, other):
        #calculate distance between two points
        return math.sqrt(abs(self.x-other.x)**2 + abs(self.y-other.y)**2)

    def shift(self, x_incr, y_incr):
        self.x += x_incr
        self.y += y_incr

    def dist_x(self, other):
        return other.x - self.x

    def dist_y(self, other):
        return other.y - self.y

    def bind(self, min_x, min_y, max_x, max_y, bounce = False): #if bounce is True, movement beyond boundary is "reflected"
        if self.x < min_x:
            self.x = min_x
            if bounce: self.x += 1
        elif self.x > max_x:
            self.x = max_x
            if bounce: self.y -= 1

        if self.y < min_y:
            self.y = min_y
            if bounce: self.y += 1
        elif self.y > max_y:
            self.y = max_y
            if bounce: self.y -= 1

    def __repr__(self):
        return "".join(["Point(", str(self.x), ",", str(self.y), ")"])

class Map:
    def __init__(self, x_max, y_max):
        self.fox_pos = Point(2,2)
        self.fox_aggro_range = 8 #distance of detection
        self.PM_DURATION = 10 #duration of powermush in ticks
        self.powermush_left = 0 #current duration of powermush left
        self.player_pos = Point(x_max,y_max)
        self.x_max = x_max
        self.y_max = y_max
        self.populate_map()
        
    def populate_map(self):
        self.mushroom_pos_arr = [Point(2,3), Point(5,9), Point(10,1), Point(2,11)]
        self.powermush_pos = Point(10,10)
        self.tree_pos_arr = [Point(5,8), Point(6,8), Point(6,9), Point(6,10), Point(6,11)]

    def fox_chase(self):
        if self.fox_detect():
            delta_x = 0 if self.fox_pos.dist_x(self.player_pos) == 0 else int(math.copysign(1.0, self.fox_pos.dist_x(self.player_pos)))
            delta_y = 0 if self.fox_pos.dist_y(self.player_pos) == 0 else int(math.copysign(1.0, self.fox_pos.dist_y(self.player_pos)))
            self.shift_fox(delta_x, delta_y) #fox calculates distance to hedgehog and tries to decrease it
        else:
            self.shift_fox(randint(-1, 1), randint(-1, 1)) #fox moves in a random direction

    def fox_run(self):
        delta_x = 0 if self.fox_pos.dist_x(self.player_pos) == self.x_max else -1*int(math.copysign(1.0, self.fox_pos.dist_x(self.player_pos)))
        delta_y = 0 if self.fox_pos.dist_y(self.player_pos) == self.y_max else -1*int(math.copysign(1.0, self.fox_pos.dist_y(self.player_pos)))
        self.shift_fox(delta_x, delta_y) #fox doesn't run if max distance is reached, else calculates distance and tries to increase it
        self.powermush_left -= 1 #decrement powermush duration

    def fox_detect(self):
        dist = self.fox_pos.dist_to(self.player_pos)
        return dist <= self.fox_aggro_range #check if distance to hedgehog is less or equal to aggro range

    def shift_fox(self, x_incr, y_incr):
        self.fox_pos.shift(x_incr,y_incr)
        self.fox_pos.bind(0, 0, self.x_max, self.y_max, True)

    def shift_player(self, x_incr, y_incr):
        self.player_pos.shift(x_incr,y_incr)
        self.player_pos.bind(0, 0, self.x_max, self.y_max)

    def remove_mushroom(self, pos):
        for m_pos in self.mushroom_pos_arr:
            if m_pos == pos:
                self.mushroom_pos_arr.remove(m_pos)

    def remove_powermushroom(self):
        self.powermush_left = self.PM_DURATION
        self.powermush_pos = None