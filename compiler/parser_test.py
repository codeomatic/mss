import unittest
from tokens import Token, TokenType
from parser_ import Parser
from nodes import *


class TestParser(unittest.TestCase):

    def test_empty(self):
        tokens = []
        tree = Parser(tokens).parse()
        self.assertEqual(tree, None)

    def test_numbers(self):
        tokens = [Token(TokenType.NUMBER, 51.2)]
        tree = Parser(tokens).parse()
        self.assertEqual(tree, NumberNode(51.2))

    def test_individual_operations(self):
        tokens = [
            Token(TokenType.NUMBER, 27),
            Token(TokenType.PLUS),
            Token(TokenType.NUMBER, 14),
        ]

        tree = Parser(tokens).parse()
        self.assertEqual(tree, AddNode(NumberNode(27), NumberNode(14)))

        tokens = [
            Token(TokenType.NUMBER, 27),
            Token(TokenType.MINUS),
            Token(TokenType.NUMBER, 14),
        ]

        tree = Parser(tokens).parse()
        self.assertEqual(tree, SubtractNode(NumberNode(27), NumberNode(14)))

        tokens = [
            Token(TokenType.NUMBER, 27),
            Token(TokenType.MULTIPLY),
            Token(TokenType.NUMBER, 14),
        ]

        tree = Parser(tokens).parse()
        self.assertEqual(tree, MultiplyNode(NumberNode(27), NumberNode(14)))

        tokens = [
            Token(TokenType.NUMBER, 27),
            Token(TokenType.DIVIDE),
            Token(TokenType.NUMBER, 14),
        ]

        tree = Parser(tokens).parse()
        self.assertEqual(tree, DivideNode(NumberNode(27), NumberNode(14)))

    def test_individual_bool_operations(self):
        tokens = [
            Token(TokenType.NUMBER, 42),
            Token(TokenType.EQ),
            Token(TokenType.NUMBER, 42),
        ]
        tree = Parser(tokens).parse()
        self.assertEqual(tree, BoolNodeEQ(NumberNode(42), NumberNode(42)))

        tokens = [
            Token(TokenType.NUMBER, 42),
            Token(TokenType.LT),
            Token(TokenType.NUMBER, 42),
        ]
        tree = Parser(tokens).parse()
        self.assertEqual(tree, BoolNodeLT(NumberNode(42), NumberNode(42)))

        tokens = [
            Token(TokenType.NUMBER, 42),
            Token(TokenType.EQLT),
            Token(TokenType.NUMBER, 42),
        ]
        tree = Parser(tokens).parse()
        self.assertEqual(tree, BoolNodeEQLT(NumberNode(42), NumberNode(42)))

        tokens = [
            Token(TokenType.NUMBER, 42),
            Token(TokenType.GT),
            Token(TokenType.NUMBER, 42),
        ]
        tree = Parser(tokens).parse()
        self.assertEqual(tree, BoolNodeGT(NumberNode(42), NumberNode(42)))

        tokens = [
            Token(TokenType.NUMBER, 42),
            Token(TokenType.GTEQ),
            Token(TokenType.NUMBER, 42),
        ]
        tree = Parser(tokens).parse()
        self.assertEqual(tree, BoolNodeGTEQ(NumberNode(42), NumberNode(42)))

    def test_full_expression(self):
        tokens = [
            # 27 + (43 / 36 - 48) * 51.2 > 27
            Token(TokenType.NUMBER, 27),
            Token(TokenType.PLUS),
            Token(TokenType.LPAREN),
            Token(TokenType.NUMBER, 43),
            Token(TokenType.DIVIDE),
            Token(TokenType.NUMBER, 36),
            Token(TokenType.MINUS),
            Token(TokenType.NUMBER, 48),
            Token(TokenType.RPAREN),
            Token(TokenType.MULTIPLY),
            Token(TokenType.NUMBER, 51),
            Token(TokenType.GT),
            Token(TokenType.NUMBER, 27)
        ]

        tree = Parser(tokens).parse()
        self.assertEqual(tree,
            BoolNodeGT(
                AddNode(
                    NumberNode(27),
                    MultiplyNode(
                        SubtractNode(
                            DivideNode(
                                NumberNode(43),
                                NumberNode(36)
                            ),
                            NumberNode(48)
                        ),
                        NumberNode(51)
                    )
                ),
                NumberNode(27)
            )
        )
