from tokens import Token, TokenType

WHITESPACE = ' \t\n'
DIGITS = '0123456789'
COMPARISON_CHARS = '=<>'


class Lexer:
    def __init__(self, text):
        self.text = iter(text)
        self.advance()

    def advance(self):
        try:
            self.current_char = next(self.text)
        except StopIteration:
            self.current_char = None

    def generate_tokens(self):
        while self.current_char != None:
            if self.current_char in WHITESPACE:
                self.advance()
            elif self.current_char in COMPARISON_CHARS:
                yield self.generate_comparison(self.current_char)
            elif self.current_char == '.' or self.current_char in DIGITS:
                yield self.generate_number()
            elif self.current_char == '+':
                self.advance()
                yield Token(TokenType.PLUS)
            elif self.current_char == '-':
                self.advance()
                yield Token(TokenType.MINUS)
            elif self.current_char == '*':
                self.advance()
                yield Token(TokenType.MULTIPLY)
            elif self.current_char == '/':
                self.advance()
                yield Token(TokenType.DIVIDE)
            elif self.current_char == '(':
                self.advance()
                yield Token(TokenType.LPAREN)
            elif self.current_char == ')':
                self.advance()
                yield Token(TokenType.RPAREN)
            else:
                raise Exception(f"Illegal character '{self.current_char}'")

    def generate_number(self):
        decimal_point_count = 0
        number_str = self.current_char
        self.advance()

        while self.current_char != None and (self.current_char == '.' or self.current_char in DIGITS):

            if self.current_char == '.':
                decimal_point_count += 1
                if decimal_point_count > 1:
                    break
            number_str += self.current_char
            self.advance()

        if number_str.startswith('.'):
            number_str = '0' + number_str
        if number_str.endswith('.'):
            number_str = number_str + '0'

        return Token(TokenType.NUMBER, float(number_str))

    def generate_comparison(self, initial_char: str) -> Token:
        """Look ahead to generate comparison tokens"""
        self.advance()
        if initial_char == '=' and self.current_char == '=':
            self.advance()
            # ==
            return Token(TokenType.EQ)

        if initial_char == '=' and self.current_char == '<':
            self.advance()
            # =<
            return Token(TokenType.EQLT)

        if initial_char == '>' and self.current_char == '=':
            self.advance()
            # >=
            return Token(TokenType.GTEQ)
    
        # At this point we didn't match anything we care about so we don't
        # advance to let other other iterations deal with current character
        if initial_char == '>':
            # >
            return Token(TokenType.GT)
        
        if initial_char == '<':
            # <
            return Token(TokenType.LT)
        
        # This will be called only when the function is called with unexpected character
        raise Exception(f"Not handled comparison character: '{initial_char}'")

