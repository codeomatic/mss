# MSS assignment

## Answers
Assignment description and answers are in [Assignment_3.pdf](Assignment_3.pdf)

## Install requirements
From project root directory run `pip install -r requirements.txt`.
Use some python environment manager like [virtualenv](https://virtualenv.pypa.io/en/latest/installation.html).

## Run tests
Run `pytest`

## Contributors

* Ilya Nokhrin
* Anton Zalaldinov
* Denis Nikolskiy
