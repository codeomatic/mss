from threading import Thread
from fox import FoxFSA
from map import Map, Point
from os import system

class Game(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event
        
        # initializing states
        self.MAX_X = 12
        self.MAX_Y = 12
        self.map = Map(self.MAX_X,self.MAX_Y)
        self.foxMachine = FoxFSA()
        self.player_move = ''

    def run(self):
        while not self.stopped.wait(2.5):
            self.check_user_action()
            self.update_map()
            self.log_state()
            self.update_game()
            
    def check_user_action(self):
        if self.player_move == 'd':
            self.map.shift_player(1,0)
        elif self.player_move == 'a':
            self.map.shift_player(-1,0)
        elif self.player_move == 'w':
            self.map.shift_player(0,-1)
        elif self.player_move == 's':
            self.map.shift_player(0,1)

        self.player_move = ''
    
    def set_user_action(self, action):
        self.player_move = action

    def update_map(self):
        if self.map.powermush_pos == None and self.map.mushroom_pos_arr == []: #no more mushrooms, victory for hedgehog
            self.foxMachine.send('v')
        elif self.map.player_pos == self.map.fox_pos:
            self.foxMachine.send('m')
        elif self.map.player_pos == self.map.powermush_pos:
            self.map.remove_powermushroom()
        elif self.map.player_pos in self.map.mushroom_pos_arr:
            self.map.remove_mushroom(self.map.player_pos)
        else:
            if self.map.powermush_left > 0:
                self.foxMachine.send('p')
            else:
                self.foxMachine.send('')

    def update_game(self):
        if self.foxState == 'Fox won' or self.foxState == 'Hedgehog won' or self.foxState == 'Stopped':
            self.stopped.set()
        elif self.foxState == 'Chasing':
            self.map.fox_chase()
        elif self.foxState == 'Running':
            self.map.fox_run()
        else:
            print('State outside of what is expected')

    def log_state(self):
        system('cls')
        for y in range(-1, self.MAX_Y+2):
            for x in range(-1, self.MAX_X+2):
                if x in [-1, self.MAX_X+1] or y in [-1, self.MAX_Y+1]:
                    print('#', end='')
                else:
                    p = Point(x, y)
                    if p == self.map.fox_pos:
                        print('F', end='')
                    elif p == self.map.player_pos:
                        print('O', end='')
                    elif p == self.map.powermush_pos:
                        print('M', end='')
                    elif p in self.map.mushroom_pos_arr:
                        print('m', end='')
                    else:
                        print('.', end='')
            print()
        self.foxState = self.foxMachine.get_state()
        print('Fox state:', self.foxState)
        print('Fox is at', self.map.fox_pos)
        print('Player is at', self.map.player_pos)
